﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoint : MonoBehaviour
{
    [SerializeField] ChainTrigger saveDataTrigger;
    [SerializeField] ChainTrigger loadDataTrigger;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (saveDataTrigger.triggered)
            {
                SaveChainData.Save(TempGameController.instance.saveFilePath);
                saveDataTrigger.triggered = false;
            }

            if (loadDataTrigger.triggered)
            {
                SaveChainData.Load(TempGameController.instance.saveFilePath);
                loadDataTrigger.triggered = false;
            }
        }
    }
}
