﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Vector3 tempPos;
    Vector3 tempScale;
    public float speed;
    public bool isMoving;

    void FixedUpdate()
    {
        tempPos = gameObject.transform.position;
        tempScale = gameObject.transform.localScale;
        bool isMovingSwitched = isMoving;

        if (!DialogueChainPreferences.GetHaltMovement())
        {
            isMoving = false;

            if (Input.GetKey(KeyCode.A))
            {
                tempPos.x -= speed * Time.deltaTime;
                tempScale.x = -1;
                isMoving = true;
            }
            if (Input.GetKey(KeyCode.W))
            {
                tempPos.y += speed * Time.deltaTime;
                isMoving = true;
            }
            if (Input.GetKey(KeyCode.S))
            {
                tempPos.y -= speed * Time.deltaTime;
                isMoving = true;
            }
            if (Input.GetKey(KeyCode.D))
            {
                tempPos.x += speed * Time.deltaTime;
                tempScale.x = 1;
                isMoving = true;
            }
            gameObject.transform.position = tempPos;
            gameObject.transform.localScale = tempScale;
        }

        if (isMoving == isMovingSwitched)
        {
            GetComponent<Animator>().SetBool("Walking", isMoving);
        }

    }
}
