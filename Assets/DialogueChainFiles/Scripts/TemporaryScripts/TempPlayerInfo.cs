﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempPlayerInfo : MonoBehaviour
{
    //Everything here is used with dialogue chains. Customize the script DialogueChainPreferences to reflect your own variables.

    public string playerName;
    public bool isMale;
    public int experience;

    public int meleeStat;
    public int rangedStat;
    public int Mentiras, SepararLixo, AjudarAdultos, Saude, Vitalidade;
    public int generic1, generic2, generic3, generic4, generic5;

    public List<Sprite> playerDialogueAvatars;
}
/*
 * 
 *   Mentiras = 2,
    SepararLixo = 3,
    AjudarAdultos = 4,
    Saude = 5,
    Vitalidade = 6
 * */
