﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGeneralController : MonoBehaviour
{
    public TempGameController myTempController;
    bool last;
    public GameObject Player;
    public TempPlayerInfo playerInfo;
    public GameObject[] controllersToDisable;
    public bool finishedChallenge;

    // Start is called before the first frame update
    void Start()
    {
        finishedChallenge = false;
        enableControllers();
        if (Player == null) Player = GameObject.FindGameObjectWithTag("Player");
        if (PlayerPrefs.GetInt("finished" ) == 1) finishedChallenge = true;
    }

    // Update is called once per frame
    void Update()
    {
        // disable Controllers on dialog
        if(myTempController.haltMovement != last)
        {
            if(myTempController.haltMovement)
            {
                disableControllers();
            }
            else if(!myTempController.haltMovement)
            {
                enableControllers();
            }
        }
        last = myTempController.haltMovement;
    }


    public void enableControllers()
    {
        foreach(GameObject g in controllersToDisable)
        {
            g.SetActive(true);
        }
    }

    public void disableControllers()
    {
        foreach (GameObject g in controllersToDisable)
        {
            g.SetActive(false);
        }
    }

}
