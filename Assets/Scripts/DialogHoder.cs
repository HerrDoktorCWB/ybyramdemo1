﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogHoder : MonoBehaviour
{
    public DialogueChain myChain;
    public TempGameController myTempController;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void callMyDialog()
    {
        if (myTempController.haltMovement) return;
        myChain.StartChain();
    }
}
