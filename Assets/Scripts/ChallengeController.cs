﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengeController : MonoBehaviour
{
    List<challengeData> currentQuests = new List<challengeData>();
    public int currentIndex;

    public CoinController coinController;

    public Text questTextField;

    public string introMessage;

    public string[] goodMessages;

    public string[] badmessages;

    public string endMessage;

    public bool finishedChallenges;

    public float messageTime;

    bool showingResult;

    // Start is called before the first frame update
    void Start()
    {
        createObjective(200, "Seu Tonico da farmácia pediu 3 maçãs, cada uma custa 1 Real. Ele me deu 5 Reais, e agora? Quanto tenho que dar de troco?");
        createObjective(80, "Dona Filomena me deu 2 Reais e pediu 4 morangos, cada um custa R$ 0,30, cheguei que dá R$ 1,20, mas o troco tá difícil! ");
        createObjective(400, "Cada melancia custa R$ 3, sua mãe me pediu duas e me deu R$10, quanto tenho que te dar mesmo?");
        createObjective(200, "O Tião do posto pediu para trocar uma nota de 2 reais em moedas! Pode me ajudar?");
        createObjective(75, "Fiz uma pêra por R$0,25 para a Mariazinha, ela me deu R$1,00, quanto devo devolver a ela?");


        questTextField.text = introMessage;
        StartCoroutine(waitForSeconds(messageTime));

       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void createObjective(int cashGoal, string text)
    {
        challengeData c = new challengeData();
        c.cashGoal = cashGoal;
        c.challengeText = text;
        c.done = false;
        currentQuests.Add(c);
    }

    public bool loadObjective()
    {
        bool test = false;
        int count = 0 ;
        while (!test)
        {
            int i = Random.Range(0, currentQuests.Count);
            challengeData c = currentQuests[1];
            if(!c.done)
            {
                test = true;
                currentIndex = i;
                return true;
            }
            else
            {
                count++;
                if (count > 2000)
                {
                    return false;
                }
            }            
        }
        return false;
    }

    public void testObjective()
    {
        if (showingResult) return;

        showingResult = true;
        //bool results = false;
        if(coinController.currentValue == currentQuests[currentIndex].cashGoal)
        {
            currentQuests[currentIndex].done = true;
            StartCoroutine(showMessage(true));
        }
        else
        {
            StartCoroutine(showMessage(false));
        }

       // return results;
    }

    public void showObjectiveText()
    {
        questTextField.text = currentQuests[currentIndex].challengeText;
    }

    IEnumerator showMessage(bool good)
    {
        if (good)
        {
            int i = Random.Range(0, goodMessages.Length);
            questTextField.text = goodMessages[i];
        }
        else
        {
            int i = Random.Range(0, badmessages.Length);
            questTextField.text = badmessages[i];
        }

        yield return new WaitForSeconds(messageTime);
             
        
        if (good)
        {
            if (!loadObjective())
            {
                //terminou as  atividades
                finishedChallenges = true;
                PlayerPrefs.SetInt("finished", 1);
                questTextField.text = endMessage;
            }
            else
            {
                showObjectiveText();
            }
            coinController.clearCoins();
        }
        else
        {
            showObjectiveText();
        }

        showingResult = false;
    }    

    IEnumerator waitForSeconds(float wait)
    {
        yield return new WaitForSeconds(wait);
        loadObjective();
        showObjectiveText();

        finishedChallenges = false;
    }
}


public class challengeData
{
    public int cashGoal;
    public string challengeText;
    public bool done;
}
