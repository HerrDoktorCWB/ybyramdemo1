﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    public Light myLight;
    public bool inRange;
    // Start is called before the first frame update
    void Start()
    {
        myLight.enabled = false;
        inRange = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.name == "Player" || other.tag == "Player")
        {
            myLight.enabled = true;
            inRange = true;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.name == "Player" || other.tag == "Player")
        {
            myLight.enabled = false;
            inRange = false;
        }
    }
}
