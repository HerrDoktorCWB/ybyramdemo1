﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class ComparePosition : MonoBehaviour
{
    Vector3 iniPos, currPos;
    public Animator targetAnimator;
    public string targetFloat;

    // Start is called before the first frame update
    void Start()
    {
        iniPos = transform.position;
        currPos = iniPos;
    }

    // Update is called once per frame
    void Update()
    {
        currPos = transform.position;
        if (!compareVectors(iniPos, currPos))
        {
            iniPos = currPos;
            targetAnimator.SetFloat(targetFloat, 10);
        }
        else
        {
            targetAnimator.SetFloat(targetFloat, 0);
        }
    }

    public bool compareVectors(Vector3 iniVector, Vector3 endVector)
    {
        bool temp = iniVector.Equals(endVector);
        //Debug.Log(temp);
        return temp;
    }
}
