﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainTriggerTester : MonoBehaviour
{
    public ChainTrigger myChainTrigger;
    ChainTrigger activeTrigger;
    public bool canLeave;
    // Start is called before the first frame update
    void Start()
    {
        canLeave = false;
        activeTrigger = myChainTrigger;
        // removi o teste por hora, nao preciso dele :P
    }

 
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger");
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("Player");
            Debug.Log(activeTrigger.triggered);
            canLeave = activeTrigger.triggered;  
        }
    }
    /*
    public void setCurrentTrigger(string triggerName)
    {
        foreach(ChainTrigger c in myChainTrigger)
        {
            if(c.name.Equals(triggerName))
            {
                activeTrigger = c;
            }
        }
    }
    */
}
