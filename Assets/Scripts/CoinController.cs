﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

using HutongGames.PlayMaker;

public class CoinController : MonoBehaviour
{
    public GameObject[] coins; // 10,25,50,100 in this order
    public Transform spawnPoint;
    
    List<GameObject> CoinSpawn10 = new List<GameObject>();
    List<GameObject> CoinSpawn25 = new List<GameObject>();
    List<GameObject> CoinSpawn50 = new List<GameObject>();
    List<GameObject> CoinSpawn100 = new List<GameObject>();


    public Text txt10, txt25, txt50, txt100;

    public float currentValue;

    public int CoinMaxSpawn;

    public PlayMakerFSM targetFsm;
    public string fsmEvent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addCoin(int coin)
    {
        GameObject tempObject = tempObject = coins[0]; ;

        switch (coin)
        {
            case 10:
                if (CoinSpawn10.Count > CoinMaxSpawn) return;
                tempObject = coins[0];
                CoinSpawn10.Add(Instantiate(tempObject, spawnPoint.transform.position, Quaternion.identity));
                Debug.Log( CoinSpawn10.Count);
                break;

            case 25:
                if (CoinSpawn25.Count > CoinMaxSpawn) return;
                tempObject = coins[1];
                CoinSpawn25.Add(Instantiate(tempObject, spawnPoint.transform.position, Quaternion.identity));
                Debug.Log(CoinSpawn25.Count);
                break;

            case 50:
                if (CoinSpawn50.Count > CoinMaxSpawn) return;
                tempObject = coins[2];
                CoinSpawn50.Add(Instantiate(tempObject, spawnPoint.transform.position, Quaternion.identity));
                Debug.Log(CoinSpawn50.Count);
                break;

            case 100:
                if (CoinSpawn100.Count > CoinMaxSpawn) return;
                tempObject = coins[3];
                CoinSpawn100.Add (Instantiate(tempObject, spawnPoint.transform.position, Quaternion.identity));
                Debug.Log(CoinSpawn100.Count);
                break;
        }

        updateTexts();
        currentValue += coin;
        targetFsm.SendEvent(fsmEvent);
    }

    public void removeCoin(int coin)
    {
        int randomPosition;
        GameObject tempObject;
        switch (coin)
        {
            case 10:
                if (CoinSpawn10.Count <1) return;
                randomPosition = Random.Range(0, CoinSpawn10.Count);
                tempObject = CoinSpawn10[randomPosition];
                CoinSpawn10.RemoveAt(randomPosition);
                Debug.Log(CoinSpawn10.Count);
                Destroy(tempObject);
                break;

            case 25:
                if (CoinSpawn25.Count <1) return;
                randomPosition = Random.Range(0, CoinSpawn10.Count);
                tempObject = CoinSpawn25[randomPosition];
                CoinSpawn25.RemoveAt(randomPosition);
                Debug.Log(CoinSpawn25.Count);
                Destroy(tempObject);
                break;

            case 50:
                if (CoinSpawn50.Count <1) return;
                randomPosition = Random.Range(0, CoinSpawn10.Count);
                tempObject = CoinSpawn50[randomPosition];
                CoinSpawn50.RemoveAt(randomPosition);
                Debug.Log(CoinSpawn50.Count);
                Destroy(tempObject);
                break;

            case 100:
                if (CoinSpawn100.Count <1) return;
                randomPosition = Random.Range(0, CoinSpawn10.Count);
                tempObject = CoinSpawn100[randomPosition];
                CoinSpawn100.RemoveAt(randomPosition);
                Debug.Log(CoinSpawn100.Count);
                Destroy(tempObject);
                break;
        }
        updateTexts();
        currentValue -= coin;
    }

    void updateTexts()
    {
        txt10.text = "" + CoinSpawn10.Count;
        txt25.text = "" + CoinSpawn25.Count;
        txt50.text = "" + CoinSpawn50.Count;
        txt100.text = "" + CoinSpawn100.Count;
    }

    public void clearCoins()
    {
        foreach(GameObject g in CoinSpawn10)
        {
            Destroy(g);
        }
        CoinSpawn10.Clear();

        foreach (GameObject g in CoinSpawn25)
        {
            Destroy(g);
        }
        CoinSpawn25.Clear();

        foreach (GameObject g in CoinSpawn50)
        {
            Destroy(g);
        }
        CoinSpawn50.Clear();

        foreach (GameObject g in CoinSpawn100)
        {
            Destroy(g);
        }
        CoinSpawn100.Clear();

        updateTexts();
        currentValue = 0;
    }

}
